import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';

declare var fabric: any;
// var fabric: any = require('fabric');

@Component({
  selector: 'editor',
  templateUrl: 'editor.component.html',
  styleUrls: [ 'editor.component.css' ]
})

export class EditorComponent implements OnInit {

imageLoader: any;
    
CanvasRep: any;
ctx: any;
fabricCanvasRep: any;
state : any[]= [];
mods = 0;

constructor( private dataservice: DataService, private router: Router) {

  
}

ngOnInit() {
this.CanvasRep = document.getElementById('imageCanvas');
this.ctx = this.CanvasRep.getContext('2d');
this.fabricCanvasRep = new fabric.Canvas('imageCanvas');

var self=this;

 this.fabricCanvasRep.on(
    'object:modified', function () {
    self.updateModifications(true);
},
    'object:added', function () {
    self.updateModifications(true);
});


  }



  updateModifications(savehistory: boolean) {
var canvasInJson: any;
    if (savehistory === true) {
    
        canvasInJson = JSON.stringify(this.fabricCanvasRep);
         this.state.push(canvasInJson); 
    }
}


addText(){
    
    var textField: any = document.getElementById('addText');

var text = this.fabricCanvasRep.add(new fabric.Text(textField.value, { 
    left: 10, 
    top: 10
}));

this.updateModifications(true);

this.fabricCanvasRep.renderAll(); 
}

moveToDesigns(){
  this.router.navigateByUrl('designs');
}


 undo() {
    if (this.mods < this.state.length) {
        this.fabricCanvasRep.clear().renderAll();
     
      this.fabricCanvasRep.loadFromJSON(this.state[this.state.length - 1 - this.mods - 1],this.fabricCanvasRep.renderAll.bind(this.fabricCanvasRep));
         this.fabricCanvasRep.renderAll();
        
        this.mods += 1;
        
    }
}

redo() {
    if (this.mods > 0) {
        this.fabricCanvasRep.clear().renderAll();
        this.fabricCanvasRep.loadFromJSON(this.state[this.state.length - 1 - this.mods + 1],this.fabricCanvasRep.renderAll.bind(this.fabricCanvasRep));
        this.fabricCanvasRep.renderAll();
        
        this.mods -= 1;
        
    }
}

save(){
var dataURL = this.CanvasRep.toDataURL();

this.dataservice.addDesign(dataURL.split(/,(.+)/)[1]);
 
  
}

handleImage(e: any){

var reader = new FileReader();
  var that = this;
    reader.onload = function(event){
   
         var img = new Image();
        img.onload = function(){
        that.fabricCanvasRep.setHeight(500);
 that.fabricCanvasRep.setWidth(500);
            
            var imgInstance = new fabric.Image(img, {
  left: 10,
  top: 10,
  opacity: 0.85
});

that.fabricCanvasRep.add(imgInstance);

that.updateModifications(true);


        }
        let temp: any = event.target
        img.src = temp.result;
       }
    reader.readAsDataURL(e.target.files[0]);     
  
}


  
}
