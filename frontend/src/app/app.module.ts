import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, Http, XHRBackend, RequestOptions } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { routing, appRoutingProviders } from './app.routing';

// custom component imports
import { EditorComponent } from './editor/editor.component';
import { DesignsComponent } from './designs/designs.component';

// service imports
import { DataService } from './services/data.service';


@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    routing ],
  declarations: [
    AppComponent,
    EditorComponent,
    DesignsComponent  
],
  providers: [
    DataService,
    appRoutingProviders
    ],
  bootstrap: [ AppComponent ]
})

export class AppModule {}
