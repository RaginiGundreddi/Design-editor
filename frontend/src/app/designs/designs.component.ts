import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';


@Component({
  selector: 'designs',
  templateUrl: 'designs.component.html',
  styleUrls: [ 'designs.component.css' ]
})

export class DesignsComponent implements OnInit {

designs: any[];



constructor( private dataservice: DataService, private router: Router) {

  
}

ngOnInit() {

this.getDesigns();


  }

  moveToEditor(){
  this.router.navigateByUrl('editor');
}

  getDesigns() {
   this.dataservice.getDesigns().subscribe( (response: any) => {this.designs= response;
  
   for(var i = 0; i < this.designs.length; i++) {

    
    var outputImg = document.createElement('img');
    outputImg.src = "data:image/png;base64, " + this.designs[i];
    document.body.appendChild(outputImg);
    }

   

  });

  }
  }