import { Injectable } from '@angular/core';
import { Headers, Http, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';


@Injectable()
export class DataService {
  

  constructor(private http: Http ) {}

  getDesigns(): Observable<Response> {
     let headers = new Headers();
  
  
     headers.append('Accept', '*/*');
  
     return this.http.get('http://localhost:3000/', { headers: headers }).map(this.extractData);

   }

  addDesign(data: any): void {
  
     let headers = new Headers();
     let body = new URLSearchParams();
     body.set('image',data);
  
  
     headers.append('Accept', '*/*');
  
     this.http.post('http://localhost:3000/',
                           body,
                           { headers: headers }).subscribe();
   }

  private extractData(response: Response) {

  return response.json();
  }


}
