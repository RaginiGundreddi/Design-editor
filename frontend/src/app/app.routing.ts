import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DesignsComponent } from './designs/designs.component';
import { EditorComponent } from './editor/editor.component';


// An array of routes that describe how to navigate
// Each Route maps a URL path to a component
const appRoutes: Routes = [
  { path: 'editor', component: EditorComponent },
  { path: 'designs', component: DesignsComponent },
  { path: '**', component: EditorComponent }

  ];

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
