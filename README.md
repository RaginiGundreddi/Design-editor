# Dessign Editor


> Development done on Windows OS, steps may
> differ if you are attempting to install using a MAC.


## Pre-installation Software Requirements
> 1. [nodejs v6.x+](https://nodejs.org/en/)
>
> 2. [git v2.10.x+](https://git-scm.com/)
>
> 3. [npm]
>
> 4. [sql]



## Global installations
>
> 1. `npm install typings -g`
> 2. `npm install webpack -g`
> 3. `npm install rimraf -g`
> 4. `npm install karma -g`




## Project Installation
> **Project Installation occurs from terminal or command prompt**
>
> 1. Clone project from gitlab
>    `git clone <project gir URL>`
>
> 2. Navigate to new project directory
>    `cd <path-to-cloned-project-directory>`
>
> 3. FRONTEND Package installation and running
>    `move to frontend folder`
>    `npm install`   (needed only for first time)
>    `npm run start`
>
> 4. Create a database named 'test' in sql
>
> 5. Create a table named 'designs' in 'test' database by running the below sql command > in sql terminal
>      CREATE TABLE test.designs (img blob NOT NULL);
>
> 6. BACKEND Package installation and running
>    `move to backend folder` (in another terminal/ command prompt)
>    `npm install`   (needed only for first time)
>    `npm run start`
>
> 7. Starting fromctend
>    `http://localhost:8080` (in a browser)
>
> 8. Perform tasks in the editor 



## Project reinstallation
> **Steps to reinstall project if project dependencies updated**
>
> 1. Navigate to project directory
>    `cd <path-to-project-direcory>`
>
> 2. Clean project directory
>    `npm run reinstall`



## Running unit test suite
> **Running unit tests after project installation**
>
> 1. Navigate to project directory
>    `cd <path-to-project-dirctory>`
>
> 2. Run karma-jasmine test suite single run
>    `npm run test`
>
> 3. Or run full karma suite
>    `npm run test:watch`
